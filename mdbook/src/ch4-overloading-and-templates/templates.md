# Templates

---

* As previously discussed, templates are how we can achieve generic programming in C++
* Templates allow a straightforward way to represent a wide range of general concepts and simple ways to combine them
* Templates allow a type or value to be a parameter in the definition of a class, function or type alias
* Every major standard-library abstraction is represented as a template
  * ostream, list, map, unique\_ptr, functions

---

### Template Function

```cpp
template<typename T>
T foo (T a, T b) {
    return a+b;
}
```

* Notice that we define a **template **and specify a **typename **labeled **T**
* We then specify that function **foo\(\) **has a return type of **T**
* We can then add together **a **and **b, **both of type **T**
* Do note, that if you are writing a template, the operators you use must be defined for the classes you are working with

### Template Class

```cpp
template<typename T, int size>
class Array {
    T[size] array_;
public:
...
    T& operator[] (int index) {
        return array(index);
    }
```

* A class generated from a template is an ordinary class
* There is no run-time mechanism or overhead involved

### Instantiation of Templates

```cpp
std::vector<int> a;
std::map<int, double> b;
std::string c;

template<typename T> T to(const std::string& number) {
    std::stringstream ss(number);
    T num;
    ss >> num;
    return num;
}

template<typename T> T add(T num1, T num2) {
    return num1 + num2;
}
```

* For classes we almost always supply template arguments
* For functions, we sometimes supply template arguments

### Errors with Templates

```cpp
template<typename Const, typename Elem>
void push_back(Const& container, const Elem& elem) {
    container.push_back(elem);
}
std::vector<int> vecInt;
int num = 0;
push_back(vecInt, 5); // this is fine
push_back(num, 5);    // Error "left of .push_back() must have class/struct/union
```

* Unfortunately, errors relating to template parameters cannont be detected until the template is used.

### Type Checking

```cpp
template<Container Cont, typename Elem>
requires
Equal_comparable<Cont::value_type, Elem>()
int find_index(Const& c, Elem e);
```

* There is currently no way to implement requirements on template parameters

### static\_assert

```cpp
template<typename Const, typename Elem>
void push_back(Cont& container, const Elem& elem) {
    static_assert(std::is_class<Cont>::value, "Cont must be a class");
    container.push_back(elem);
}

...
push_back(num, 5); // Error: Cont must be a class
```

* static\_assert allowing for better error messages
  * static\_assert is a compile time assert
  * If false the message in the assert will appear as a compiler error

### Member templates

```cpp
class foo {
    int count_ = 0;
public:
    template<typename T>
    void accumulate(T value) {
        count_ += value;
    }
};
```

* A template or non-template class can have templated member functions. 

### Overloading Function Templates

```cpp
template<typename T>
T foo(const T& element1, const T& element2);

template<typename T>
T foo(const T& element1, int elem2);

template<typename T>
std::vector<T> foo(const std::vector<T>& element1, const T& elem2);

int foo(int elem1, int elem2);
std::vector<FooBar> foobars;
foo(1, 2);                // int foo(int, int);
foo(1.2, 2);              // T foo (const T&, int);
foo(foobars, FooBar());   // std::vector<T>
foo(const std::vector<T>&, const T&);
foo(2.3, 2LL);            // T foo(const T&, const T&);
foo('c', 1);
```

* Overload resolution will be needed to deduce the proper function call
* The most specialized function will be called

### Function Template Deduction

```cpp
template<typename T>
T max(T, T);

const int s = 7;

void k() {
    max(1,2);        // max<int>(1,2)
    max('a', 'b');   //max<char>('a', 'b')
    max(2.7, 4.9);   //max<double>(2.7, 4.9)
    max(s, 7);       //max<int>(int{s}, 7) (Trivial conversion used)

    max('a', 1);     //error: ambiguous: max<char, char>() or max<int, int>()?
    max(2.7, 4);     //error: ambiguous: max<double, double>() or max<int,int>()?
```



