# C++ Programming

---

![](assets/cpp.png)

## Topics:

* Intro to C++
* The C++ STL
* OOP
* Overloading and Templates
* Resource Management

## Resources:

#### [Documentation](http://www.cplusplus.com/doc/tutorial/)

#### [Language Reference](http://en.cppreference.com/w/)

#### [Gitlab Repo](https://gitlab.com/wstaud/CPP-Programing-2018)



