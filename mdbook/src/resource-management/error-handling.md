# Error Handling

---

### Recap Error Handling Techniques

* Terminate the program: pretty drastic approach...

```cpp
if(some_error) exit(1);
```

* Return an error value: Not always feasible. Old-C style

```cpp
int getInt(); -> int getInt(int& out);
```

* Return a legal value and leave the program in the error state
  * Set the variable to **errno** to indicate error
  * We forget to check...
* Call an error handling function
  * Unless error handler can fully handle the error, we should still terminate

```cpp
if(something wrong) something_handler();
```



